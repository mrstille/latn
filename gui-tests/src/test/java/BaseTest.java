import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import util.WebDriverFactory;


public class BaseTest {
    protected WebDriver driver;

    @BeforeTest
    public void setup() {
        this.driver = WebDriverFactory.getWebDriver();
    }

    @AfterTest
    public void teardown() {
        driver.quit();
    }
}
