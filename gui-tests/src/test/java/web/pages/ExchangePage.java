package web.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import util.Helper;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class ExchangePage extends BasePage {

    final String priceSellExactLine = "//div[starts-with(@data-tid,'field__order_book_line_sell_%s')]/div";

    By middlePrice = By.xpath("//div[@data-tid='field__order_book_dragger_price']");
    By priceSell = By.xpath("//div[starts-with(@data-tid,'field__order_book_line_sell_')]");
    By priceBuy = By.xpath("//div[@data-tid='field__order_book_line_buy_0']/div");
    By loginBtn = By.xpath("//div[contains(text(),'Log in to trade')]");
    By modalLoginContinueBtn = By.xpath("//button[@data-tid='button__continue']");

    public ExchangePage(WebDriver driver) {
        super(driver);
    }

    public boolean isExchangePageOpened(String pairName){
        return driver.getCurrentUrl().contains("exchange/"+pairName);
    }

    public ExchangePage getSpreadValueAndAssert(){
        waitUntilClickable(middlePrice, 10);
        List<WebElement> sellsItems = driver.findElements(priceSell);
        String ps = priceSellExactLine.replaceFirst("%s", String.valueOf((sellsItems.size() - 1)));
        waitUntilClickable(By.xpath(ps), 10);
        WebElement sell = driver.findElement(By.xpath(ps));
        Double lowestSellPrice = Helper.parseToDouble(sell.getText());
        assertThat(lowestSellPrice).as("Error when parsing lowestSellPrice [%s]", lowestSellPrice).isNotNull();
        WebElement buy = driver.findElement(priceBuy);
        Double highestBuyPrice = Helper.parseToDouble(buy.getText());
        assertThat(highestBuyPrice).as("Error when parsing highestBuyPrice [%s]", highestBuyPrice).isNotNull();
        assertThat(lowestSellPrice - highestBuyPrice > 0)
                .as("The difference between lowestSellPrice  [%s] and highestBuyPrice [%s] price are less then 0", lowestSellPrice, highestBuyPrice)
                .isTrue();
        return this;
    }

    public boolean isLoginToTraderWindowOpened() {
        waitAndClick(loginBtn);
        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(modalLoginContinueBtn)).isDisplayed();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            waitUntilClickable(modalLoginContinueBtn, 5);
            return false;
        }
    }


}
