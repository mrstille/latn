import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public abstract class BaseAPI {
        final RequestSpecification REQ_SPEC = new RequestSpecBuilder()
            .setBaseUri("https://api.latoken.com")
            .setBasePath(getBasePath())
            .setContentType(ContentType.JSON)
            .build();
    ObjectMapper objectMapper = new ObjectMapper();

    abstract protected String getBasePath();
}
