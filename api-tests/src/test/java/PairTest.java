import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.http.Cookies;
import org.testng.annotations.Test;
import pojos.Pair;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class PairTest extends BaseAPI {

    @Override
    protected String getBasePath() {
        return "/v2/pair";
    }

    @Test
    public void tesPairHasNoDuplicates() {
        List<Pair> pairs = getPairsStep();
        Set<Pair> doubledPairs = new HashSet<>();
        pairs.stream().reduce((p1, p2) -> {
            if (p1.equalsByCurrencies(p2)) {
                doubledPairs.add(p1);
                doubledPairs.add(p2);
            }
            return p1;
        });
        assertThat(doubledPairs).as("Found duplicates of  total pairs %s , listed below", doubledPairs.size(), doubledPairs).isEmpty();
    }


    @Test
    void testPairsHasBadTick() throws JsonProcessingException {
        List<Pair> pairs = getPairsStep().stream().filter(p -> p.getPriceTick() < 0.01D).collect(Collectors.toList());
        assertThat(pairs)
                .as("Found " + pairs.size() + " pairs with PriceTick < 0.01D:: \r\n " + objectMapper.writeValueAsString(pairs))
                .isEmpty();
    }

    @Test
    public void testCookiesAreValid() {
        Cookies cookies = given().spec(REQ_SPEC).when().get().getDetailedCookies();
        cookies.forEach(System.out::println);
        assertThat(cookies).as("There are no any cookies in server response").isNotEmpty();
    }

    // UserSteps
    private List<Pair> getPairsStep() {
        return given().spec(REQ_SPEC).when()
                .get()
                .then().statusCode(200)
                .extract().jsonPath().getList("", Pair.class);
    }

}
