package web.pages;

import org.openqa.selenium.*;

public class LandingPage extends BasePage {
    private final String currencyLineTmpl  = "//h6[normalize-space()='%s']";
    private By videoIframe = By.xpath("//iframe[@id='LAVCTV']");
    private By widgetCloseBtn = By.xpath("//*[@id='widget']/div/div[1]/div[3]");
    private By searchCss = By.cssSelector("[placeholder='Search']");

    public LandingPage(WebDriver driver){
        super(driver);
    }

    public LandingPage searchPairInTable(String firstPairName){
        driver.get(BASE_URL);
        waitUntilClickable(videoIframe,15);
        closePlayer();
        scrollPageDownUntilFindLocator(searchCss);
        driver.findElement(searchCss).sendKeys(firstPairName);
        return this;
    }
     public ExchangePage clickPairInTable(String fullPairName){
           waitAndClick(By.xpath(currencyLineTmpl.replaceFirst("%s",fullPairName)));
           return new ExchangePage(driver);
     }
    private void scrollPageDownUntilFindLocator(By locator){
        int height = 250;
        for (int i = 0; i < 10; i++) {
            try {
                waitUntilClickable(locator,3);
                break;
            }catch (TimeoutException e){}
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,"+height+")");
            height+=250;
        }

    }
    private void scrollToElement(By locator){
        waitUntilClickable(locator);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(locator));
    }
    private void closePlayer(){
        waitAndClick(videoIframe, 15);
        driver.switchTo().frame(driver.findElement(videoIframe));
        waitAndClick(widgetCloseBtn);
        driver.switchTo().defaultContent();
    }

}