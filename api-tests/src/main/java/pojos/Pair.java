package pojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;
 
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pair {
    private String id;
    private String status;
    private String baseCurrency;
    private String quoteCurrency;
    private double priceTick;
    private int priceDecimals;
    private double quantityTick;
    private int quantityDecimals;
    private int costDisplayDecimals;
    private long created;
    private float minOrderQuantity;
    private long maxOrderCostUsd;
    private float minOrderCostUsd;
    private String externalSymbol;

    public Pair(String id, String baseCurrency, String quoteCurrency) {
        this.baseCurrency = baseCurrency;
        this.quoteCurrency = quoteCurrency;
        this.id = id;
    }

    public boolean equalsByCurrencies(Pair obj) {
        return this.quoteCurrency.equalsIgnoreCase(obj.getQuoteCurrency()) && this.baseCurrency.equalsIgnoreCase(obj.getBaseCurrency());

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair) o;
        return Double.compare(pair.priceTick, priceTick) == 0 && priceDecimals == pair.priceDecimals && Double.compare(pair.quantityTick, quantityTick) == 0 && quantityDecimals == pair.quantityDecimals && costDisplayDecimals == pair.costDisplayDecimals && created == pair.created && Float.compare(pair.minOrderQuantity, minOrderQuantity) == 0 && maxOrderCostUsd == pair.maxOrderCostUsd && Float.compare(pair.minOrderCostUsd, minOrderCostUsd) == 0 && id.equals(pair.id) && status.equals(pair.status) && baseCurrency.equals(pair.baseCurrency) && quoteCurrency.equals(pair.quoteCurrency) && externalSymbol.equals(pair.externalSymbol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status, baseCurrency, quoteCurrency, priceTick, priceDecimals, quantityTick, quantityDecimals, costDisplayDecimals, created, minOrderQuantity, maxOrderCostUsd, minOrderCostUsd, externalSymbol);
    }

}
