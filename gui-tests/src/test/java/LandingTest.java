import org.testng.annotations.Test;
import web.pages.LandingPage;

import static org.assertj.core.api.Assertions.assertThat;

public class LandingTest extends BaseTest {


    @Test
    public void landingTest() {
       assertThat(new LandingPage(driver)
                .searchPairInTable("STORJ")
                .clickPairInTable("STORJ/USDT")
                .getSpreadValueAndAssert()
                .isLoginToTraderWindowOpened()).isTrue();

    }
}
