package web.pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

import java.time.Duration;

public class BasePage {
    protected static final String BASE_URL = "http://latoken.com";
    protected static final Duration DEFAULT_TIMEOUT_SEC = Duration.ofSeconds(10);
    protected WebDriver driver;
    protected WebDriverWait wait;


    public BasePage(WebDriver driver) {
        WebDriverManager.chromedriver().browserVersion("96.0.4664.45").setup();
        this.driver = driver;
        driver.manage().window().maximize();
        this.wait = new WebDriverWait(driver, DEFAULT_TIMEOUT_SEC);
       /* driver =  new ChromeDriver();
        driver.manage().window().fullscreen();
        driver.manage().window().maximize();*/
    }

    protected void waitAndClick(By locator, int sec) {
        WebDriverWait customWait = new WebDriverWait(driver, Duration.ofSeconds(sec));
        customWait.until(ExpectedConditions.elementToBeClickable(locator)).click();
    }

    protected void waitAndClick(By locator) {
        wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
    }

    protected void waitUntilClickable(By locator, int sec) {
        WebDriverWait customWait = new WebDriverWait(driver, Duration.ofSeconds(sec));
        customWait.until(ExpectedConditions.elementToBeClickable(locator));
    }
    protected void waitUntilClickable(By locator) {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    @AfterClass
    public void tearDown() {
        driver.close();
    }

}
