package util;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class WebDriverFactory {
    public static WebDriver getWebDriver(){
        WebDriverManager.chromedriver().browserVersion("96.0.4664.45").setup();
        return  new ChromeDriver();
    }
}
